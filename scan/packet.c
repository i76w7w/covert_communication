#include <stdio.h>
#include <winsock2.h>
#include <packet.h>
void icmp_make_data(char *icmp_data, int data_size, int sequence){
	ICMP_hdr *icmp_header;
	char *data_buf;
	int data_len;
	int fill_count = sizeof(icmp_rand_data) / sizeof(icmp_rand_data[0]);

	//fill in the payload
	data_buf = icmp_data + sizeof(ICMP_hdr);
	data_len = data_size - sizeof(ICMP_hdr);
	while (data_len > fill_count){
		memcpy(data_buf, icmp_rand_data, fill_count);
		data_len -= fill_count;
	}
	if (data_len > 0)
		memcpy(data_buf, icmp_rand_data, data_len);

	//fill in the header
	icmp_header = (ICMP_hdr*)icmp_data;
	icmp_header->type = ICMP_TYPE_ECHO;
	icmp_header->code = 0;
	icmp_header->id = (unsigned short)GetCurrentProcessId();
	icmp_header->checksum = 0;
	icmp_header->seq = sequence;
	icmp_header->timestamp = GetTickCount();

	icmp_header->checksum = checksum((unsigned short*)icmp_data, data_size);
}

ICMP_packet *icmp_parse_packet(ICMP_packet *icmp_pack,char *buf, int buf_len, struct sockaddr_in *from){
	icmp_pack->ip_hdr = (IP_hdr *)buf;
	icmp_pack->ip_hdr_len = (icmp_pack->ip_hdr->vers_len & 0xf) << 2; /* IP 首部长度 */

	if (buf_len < icmp_pack->ip_hdr_len + ICMP_MIN_LEN){
		printf("[Ping] Too few bytes from %s\n", inet_ntoa(from->sin_addr));
		return NULL;
	}

	icmp_pack->icmp_hdr = (ICMP_hdr*)(buf + icmp_pack->ip_hdr_len);
	icmp_pack->icmp_len = ntohs(icmp_pack->ip_hdr->total_len) - icmp_pack->ip_hdr_len;

	/* 检查校验和 */
	if (checksum((unsigned short *)icmp_pack->icmp_hdr, icmp_pack->icmp_len)){
		printf("[Ping] icmp checksum error!\n");
		return NULL;
	}

	/* 检查 ICMP 类型 */
	if (icmp_pack->icmp_hdr->type != ICMP_TYPE_ECHO_REPLY){
		printf("[Ping] not echo reply : %d\n", icmp_pack->icmp_hdr->type);
		return NULL;
	}

	/* 检查 ICMP 的 ID */
	if (icmp_pack->icmp_hdr->id != (unsigned short)GetCurrentProcessId()){
		printf("[Ping] someone else's message!\n");
		return NULL;
	}
	return icmp_pack;
}

unsigned short checksum(unsigned short *buf, int buf_len){
	unsigned long checksum = 0;
	while (buf_len > 1){
		checksum += *buf++;
		buf_len -= sizeof(unsigned short);
	}
	if (buf_len){
		checksum += *(unsigned char *)buf;
	}
	checksum = (checksum >> 16) + (checksum & 0xffff);
	checksum += (checksum >> 16);
	return (unsigned short)(~checksum);
}