#include <stdio.h>
#include <winsock2.h>
#include <packet.h>
#include <prepare.h>
int main(int argc, char **argv){
	/*used for input*/
	ICMP_user_opt option = {ICMP_DEF_TIMEOUT,
	                        5000,
							NULL,
							NULL};
	unsigned int ip_addr_min = 0,ip_addr_max=0,ip_addr;

	/*used for generating*/
	CONNECT connects={ 0 };
	ICMP_packet icmp_pack = { 0 };
	int from_addr_len = sizeof(connects.from_addr[0]);
	unsigned short seq_no = 0;
	int i = 0, j = 0, k = 0;

	/*products*/
	char *send_buf,*recv_buf;
	int send_buf_size,recv_buf_size,send_stat,recv_stat;
	unsigned int ipid[ICMP_MAX_CON][2] = { 0 };

	if (argc < 3 || argc >7)
		help(argv[0]);

	/*input*/
	parse_parameter(argc, argv,&option);
	setup_winsock();

	resolve_ip_address(&ip_addr_min, option.host1);
	resolve_ip_address(&ip_addr_max, option.host2);
	if (htonl(ip_addr_min) > htonl(ip_addr_max)){
		ip_addr_min = ip_addr_min ^ip_addr_max;
		ip_addr_max = ip_addr_min^ip_addr_max;
		ip_addr_min = ip_addr_min^ip_addr_max;
	}

	/*setup connects*/
	for (i = 0, ip_addr = ip_addr_min; htonl(ip_addr)<=htonl(ip_addr_max); i++){
		create_socket(&(connects.icmp_soc[i]));
		set_socket(&(connects.icmp_soc[i]), &(connects.dest_addr[i]),ip_addr,option.timeout);
		ip_addr = htonl(htonl(ip_addr) + 1);
	}
	j = i;
	
	send_buf_size = sizeof(ICMP_hdr) - sizeof(long);
	recv_buf_size = sizeof(ICMP_hdr) + sizeof(IP_hdr);
	send_buf = malloc(send_buf_size);
	recv_buf = malloc(recv_buf_size);

	printf("Scaning from %s", inet_ntoa(connects.dest_addr[0].sin_addr));
	printf(" to %s :\n",inet_ntoa(connects.dest_addr[j - 1].sin_addr));

	/*Send and Receive ICMP packet*/
	while (k < 2){
		for (i = 0; i < j; i++){
			icmp_make_data(send_buf, send_buf_size, seq_no++);
			//SEND
			send_stat = sendto(connects.icmp_soc[i],
				              send_buf,
							  send_buf_size,
							  0,
							  (struct sockaddr*)&(connects.dest_addr[i]),
							  sizeof(connects.dest_addr[i]));
			if (send_stat == SOCKET_ERROR){
				printf("Send to %s Failed!\n", inet_ntoa(connects.dest_addr[i].sin_addr));
				continue;
			}
			//RECEIVE
			recv_stat = recvfrom(connects.icmp_soc[i],
				                recv_buf,
								recv_buf_size,
								0,
								(struct sockaddr*)&(connects.from_addr[i]),
								&from_addr_len);
			if (recv_stat == SOCKET_ERROR)
				printf("Recv from %s Failed!\n", inet_ntoa(connects.dest_addr[i].sin_addr));
			else{
				icmp_parse_packet(&icmp_pack, recv_buf, recv_stat, &(connects.from_addr[i]));
				ipid[i][k] = htons(icmp_pack.ip_hdr->id);
			}
		}
		if(k==0)
			Sleep(option.wait);
		k++;
	}

	/*output*/
	for (i = 0; i < j; i++){
		printf("ipid of %s : %6d %6d , %4f per second.\n",
			  inet_ntoa(connects.dest_addr[i].sin_addr),
			  ipid[i][0],
			  ipid[i][1],
			  (ipid[i][1] - ipid[i][0]) / (option.wait/1000.0));
	}	
//	free(send_buf);
//	free(recv_buf);
	for (i = 0; i < j; i++)
		closesocket(connects.icmp_soc[i]);
	WSACleanup();
	return 0;
}