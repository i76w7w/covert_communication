#include <winsock2.h>
/* ICMP 类型 */
#define ICMP_TYPE_ECHO          8
#define ICMP_TYPE_ECHO_REPLY    0
#define ICMP_MIN_LEN            8  /* ICMP 最小长度, 只有首部 */
#define ICMP_DEF_COUNT          4  /* 缺省数据次数 */
#define ICMP_DEF_SIZE          32  /* 缺省数据长度 */
#define ICMP_DEF_TIMEOUT     1000  /* 缺省超时时间, 毫秒 */
#define ICMP_MAX_SIZE       65500  /* 最大数据长度 */
#define ICMP_MAX_CON        255    /*最大处理连接数*/

/* IP 首部 -- RFC 791 */
typedef struct{
	unsigned char vers_len;     /* 版本和首部长度 */
	unsigned char tos;          /* 服务类型 */
	unsigned short total_len;   /* 数据报的总长度 */
	unsigned short id;          /* 标识符 */
	unsigned short frag;        /* 标志和片偏移 */
	unsigned char ttl;          /* 生存时间 */
	unsigned char proto;        /* 协议 */
	unsigned short checksum;    /* 校验和 */
	unsigned int sour;          /* 源 IP 地址 */
	unsigned int dest;          /* 目的 IP 地址 */
}IP_hdr;

/* ICMP 首部 -- RFC 792 */
typedef struct{
	unsigned char type;         /* 类型 */
	unsigned char code;         /* 代码 */
	unsigned short checksum;    /* 校验和 */
	unsigned short id;          /* 标识符 */
	unsigned short seq;         /* 序列号 */

	/* 这之后的不是标准 ICMP 首部, 用于记录时间 */
	unsigned long timestamp;
}ICMP_hdr;

/*structure of icmp packet*/
typedef struct{
	IP_hdr *ip_hdr;
	ICMP_hdr *icmp_hdr;
	unsigned short ip_hdr_len;
	unsigned short icmp_len;
}ICMP_packet;


typedef struct{
	unsigned int timeout;  /* 等待答复的超时时间   */
	unsigned int wait;
	char *host1;
	char *host2;
}ICMP_user_opt;

typedef struct{
	int flag;
	int p_to_byte;
	int p_to_bit;
	char *check;
}SYNC;
typedef struct{
	int recv_bit;
	int count_bit;
	char recv_char;
	unsigned short ipid;
}RECV;
typedef struct{
	SOCKET icmp_soc[ICMP_MAX_CON];
	struct sockaddr_in dest_addr[ICMP_MAX_CON],from_addr[ICMP_MAX_CON];
}CONNECT;
static char icmp_rand_data[63] = "abcdefghigklmnopqrstuvwxyz0123456789"
"ABCDEFGHIJKLMNOPQRSTUVWXYZ";

void icmp_make_data(char *icmp_data, int data_size, int sequence);
ICMP_packet *icmp_parse_packet(ICMP_packet *icmp_pack, char *buf, int buf_len, struct sockaddr_in *from);
unsigned short checksum(unsigned short *buf, int buf_len);