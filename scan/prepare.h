void setup_winsock();
void create_socket(SOCKET *icmp_soc);
void set_socket(SOCKET *icmp_soc, struct sockaddr_in *dest_addr, unsigned int ip_addr, unsigned int user_timeout);
void resolve_ip_address(unsigned int *ip_addr, char *host);
void parse_parameter(int argc, char **argv,ICMP_user_opt *option);
void make_message(char message[100],SYNC*ctl_sync);
void help(char *prog_name);