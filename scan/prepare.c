#include<winsock2.h>
#include<stdio.h>
#include<packet.h>
#include<prepare.h>
void setup_winsock(){
	WSADATA wsadata;
	WSAStartup(MAKEWORD(2, 0), &wsadata);
}

void create_socket(SOCKET *icmp_soc){
	*icmp_soc = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if (*icmp_soc == INVALID_SOCKET){
		printf("[PING] socket() failed: %d\n", WSAGetLastError());
		exit(-1);
	}
}

void set_socket(SOCKET *icmp_soc, struct sockaddr_in *dest_addr,unsigned int ip_addr,unsigned int user_timeout){
	unsigned int timeout = user_timeout;
	setsockopt(*icmp_soc, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout));
	setsockopt(*icmp_soc, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout));
	memset(dest_addr, 0, sizeof(dest_addr));
	dest_addr->sin_family = AF_INET;
	dest_addr->sin_addr.s_addr = ip_addr;
}

void resolve_ip_address(unsigned int *ip_addr,char *host){
	struct hostent *host_ent = NULL; //content of a host
	*ip_addr = inet_addr(host);
	if (*ip_addr == INADDR_NONE){ //user_opt.host is a ip address or domain name
		host_ent = gethostbyname(host);
		if (!host_ent){
			printf("[PING] Fail to resolve %s\n",host);
			exit(-1);
		}
		memcpy(ip_addr, host_ent->h_addr_list[0], host_ent->h_length);
	}
}

void help(char *prog_name)
{
	printf("Usage:\n");
	printf("%s address1 address2 [-t timeout -w time_to_wait]\n", prog_name);
	printf("address1 and address2 can be ip address or domain name.\n");
	printf("timeout refers to the time waiting for icmp replying.\n");
	printf("time_to_wait refers to the time before sending the second ping.\n");
	exit(-1);
}

void parse_parameter(int argc, char **argv,ICMP_user_opt *option){
	int i;
	for (i = 1; i < argc; i++){
		if (argv[i][0] != '-'){
			option->host1 = argv[i++];
			option->host2 = argv[i];
		}
		else if (argv[i][1] == 't')
			option->timeout = atoi(argv[++i]);
		else if (argv[i][1] == 'w')
			option->wait = atoi(argv[++i]);
		else
			help(argv[0]);
	}
}
void make_message(char message[100], SYNC*ctl_sync){
	int i;
	int str_len=0;
	while (ctl_sync->check[++str_len]);
	message[0] = 0xff;
	for (i = 0; i < str_len; i++)
		message[i+1] = ctl_sync->check[i];
	printf("Enter your message:");
	scanf("%s", message + i+1);
	return;
}