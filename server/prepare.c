#include<winsock2.h>
#include<stdio.h>
#include<packet.h>
#include<prepare.h>
void setup_winsock(){
	WSADATA wsadata;
	WSAStartup(MAKEWORD(2, 0), &wsadata);
}

void create_socket(SOCKET *icmp_soc){
	*icmp_soc = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if (*icmp_soc == INVALID_SOCKET){
		printf("[PING] socket() failed: %d\n", WSAGetLastError());
		exit(-1);
	}
}

void set_socket(SOCKET *icmp_soc, struct sockaddr_in *dest_addr, icmp_user_opt* user_opt, unsigned int ip_addr){
	unsigned int timeout = user_opt->timeout;
	setsockopt(*icmp_soc, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout));

	timeout = 1000;
	setsockopt(*icmp_soc, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout));

	memset(dest_addr, 0, sizeof(dest_addr));
	dest_addr->sin_family = AF_INET;
	dest_addr->sin_addr.s_addr = ip_addr;
}

void resolve_ip_address(unsigned int *ip_addr, icmp_user_opt* user_opt){
	struct hostent *host_ent = NULL; //content of a host
	*ip_addr = inet_addr(user_opt->host);
	if (*ip_addr == INADDR_NONE){ //user_opt.host is a ip address or domain name
		host_ent = gethostbyname(user_opt->host);
		if (!host_ent){
			printf("[PING] Fail to resolve %s\n", user_opt->host);
			exit(-1);
		}
		memcpy(ip_addr, host_ent->h_addr_list[0], host_ent->h_length);
	}
}

void help(char *prog_name)
{
	char *file_name;

	file_name = strrchr(prog_name, '\\');
	if (file_name != NULL)
		file_name++;
	else
		file_name = prog_name;

	/* 显示帮助信息 */
	printf(" usage:     %s host_address [-t] [-n count] [-l size] "
		"[-w timeout]\n", file_name);
	printf(" -t         Ping the host until stopped.\n");
	printf(" -n count   the count to send ECHO\n");
	printf(" -l size    the size to send data\n");
	printf(" -w timeout timeout to wait the reply\n");
	exit(1);
}

void parse_parameter(int argc, char **argv, icmp_user_opt *user_opt_g, SYNC *ctl_sync){
	int i;
	for (i = 1; i < argc; i++){
		if ((argv[i][0] != '-') && (argv[i][0] != '/')){
			/* 处理主机名 */
			if (user_opt_g->host)
				help(argv[0]);
			else{
				user_opt_g->host = argv[i];
				continue;
			}
		}
		switch (tolower(argv[i][1])){
		case 'm':
			ctl_sync->check = argv[++i];
			break;
		case 't':   /* 持续 Ping */
			user_opt_g->persist = 1;
			break;

		case 'n':   /* 发送请求的数量 */
			i++;
			user_opt_g->count = atoi(argv[i]);
			break;

		case 'l':   /* 发送数据的大小 */
			i++;
			user_opt_g->size = atoi(argv[i]);
			if (user_opt_g->size > ICMP_MAX_SIZE)
				user_opt_g->size = ICMP_MAX_SIZE;
			break;

		case 'w':   /* 等待接收的超时时间 */
			i++;
			user_opt_g->timeout = atoi(argv[i]);
			break;

		default:
			help(argv[0]);
			break;
		}
	}
}
void make_message(char message[100], SYNC*ctl_sync){
	int i;
	int str_len=0;
	while (ctl_sync->check[++str_len]);
	message[0] = 0xff;
	for (i = 0; i < str_len; i++)
		message[i+1] = ctl_sync->check[i];
	printf("Enter your message:");
	scanf("%s", message + i+1);
	return;
}