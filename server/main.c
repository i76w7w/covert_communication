#include <stdio.h>
#include <winsock2.h>
#include <packet.h>
#include <prepare.h>
#include <handle_recv.h>

int main(int argc, char **argv){
	SYSTEMTIME sys_time;
	SOCKET icmp_soc;
	unsigned int ip_addr = 0;
	struct sockaddr_in dest_addr,from_addr;
	int from_addr_len = sizeof(from_addr);
	char *send_buf,*recv_buf;
	int send_buf_size,recv_buf_size,send_stat,recv_stat;
	SYNC ctl_sync = {
		0,   //flag
		0,   //pointer to byte
		0,   //pointer to bit
		"xdu"//password
	};
	icmp_user_opt user_opt = {
		0,               //continue ping 
		ICMP_DEF_COUNT,  //amount of echos to send
		0,               //size of the data
		ICMP_DEF_TIMEOUT,//timeout for waiting for reply
		NULL,            //address of the host
		0,				 //amount for sending
		0,               //amount for receving
		0xFFFF,          //shortest time
		0		         //longest time
	};
	RECV recv = {0,0,0,0};
	unsigned short seq_no = 0;
	int i=0,j=0,synctime = 1000;
	char message[100] = { 0 },m=8;

	if (argc < 2)
		help(argv[0]);

	parse_parameter(argc, argv, &user_opt,&ctl_sync);
	make_message(message,&ctl_sync);
	setup_winsock();
	create_socket(&icmp_soc);
	resolve_ip_address(&ip_addr, &user_opt);
	set_socket(&icmp_soc,&dest_addr,&user_opt,ip_addr);

	send_buf_size = user_opt.size + sizeof(icmp_hdr) - sizeof(long);
	recv_buf_size = user_opt.size + sizeof(icmp_hdr) + sizeof(ip_hdr);
	send_buf = malloc(send_buf_size);
	recv_buf = malloc(recv_buf_size);
	

	printf("Sync Time:");
	scanf("%d", &synctime);

	while (message[i]){
		printf("Sending %c:", message[i]);
		for (m = 7; m >= 0; m--){
			if ((message[i] >> (7 - m)) & 1){
				/*Send and Receive ICMP packet*/
				for (j = 0; j < user_opt.count; user_opt.persist ? j : j++){
					icmp_make_data(send_buf, send_buf_size, seq_no++);
					send_stat = sendto(icmp_soc, send_buf, send_buf_size, 0, (struct sockaddr*)&dest_addr, sizeof(dest_addr));
					recv_stat = recvfrom(icmp_soc, recv_buf, recv_buf_size, 0, (struct sockaddr*)&from_addr, &from_addr_len);
				}
				printf("1");
			}
			else
				printf("0");
			GetLocalTime(&sys_time);
			Sleep((sys_time.wMilliseconds / 1000) * 1000 + 1000 - sys_time.wMilliseconds);
		}
		i++;
		printf("\n");
	}

/*
		if (send_stat == SOCKET_ERROR){
			if (WSAGetLastError() == WSAETIMEDOUT){
				printf("[PING] sendto is timeout\n");
				continue;
			}
			printf("[PING] sendto failed: %d\n", WSAGetLastError());
			break;
		}
		user_opt.send++;
		
		recv_stat = recvfrom(icmp_soc,recv_buf,recv_buf_size, 0, (struct sockaddr*)&from_addr, &from_addr_len);

		if (recv_stat == SOCKET_ERROR){
			if (WSAGetLastError() == WSAETIMEDOUT){
				printf("timed out\n");
				continue;
			}
			printf("[PING] recvfrom_ failed: %d\n", WSAGetLastError());
			break;
		}
		
		recv.recv_bit=icmp_parse_reply(recv_buf, recv_stat, &from_addr, &(recv.ipid));

		if (!ctl_sync.flag){
			sync(&recv, &ctl_sync);
			printf(".");
		}
		else{
			if (!recv.count_bit){
				printf("Receving: ");
			}
			printf("%d", recv.recv_bit);
			handle_recv(&recv);
		}
*/

//	free(send_buf);
//	free(recv_buf);
	closesocket(icmp_soc);
	WSACleanup();
	return 0;
}