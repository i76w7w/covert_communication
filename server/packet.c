#include <stdio.h>
#include <winsock2.h>
#include <packet.h>
void icmp_make_data(char *icmp_data, int data_size, int sequence){
	icmp_hdr *icmp_header;
	char *data_buf;
	int data_len;
	int fill_count = sizeof(icmp_rand_data) / sizeof(icmp_rand_data[0]);

	//fill in the payload
	data_buf = icmp_data + sizeof(icmp_hdr);
	data_len = data_size - sizeof(icmp_hdr);
	while (data_len > fill_count){
		memcpy(data_buf, icmp_rand_data, fill_count);
		data_len -= fill_count;
	}
	if (data_len > 0)
		memcpy(data_buf, icmp_rand_data, data_len);

	//fill in the header
	icmp_header = (icmp_hdr *)icmp_data;
	icmp_header->type = ICMP_TYPE_ECHO;
	icmp_header->code = 0;
	icmp_header->id = (unsigned short)GetCurrentProcessId();
	icmp_header->checksum = 0;
	icmp_header->seq = sequence;
	icmp_header->timestamp = GetTickCount();

	icmp_header->checksum = checksum((unsigned short*)icmp_data, data_size);
}

int icmp_parse_reply(char *buf, int buf_len, struct sockaddr_in *from, unsigned short*ipid){
	ip_hdr *ip_hdr;
	icmp_hdr *icmp_hdr;
	unsigned short ip_hdr_len;
	int icmp_len;
	int recv_ipid, recv_data;

	ip_hdr = (struct ip_hdr *)buf;
	ip_hdr_len = (ip_hdr->vers_len & 0xf) << 2; /* IP 首部长度 */

	if (buf_len < ip_hdr_len + ICMP_MIN_LEN){
		printf("[Ping] Too few bytes from %s\n", inet_ntoa(from->sin_addr));
		return -1;
	}

	icmp_hdr = (struct icmp_hdr *)(buf + ip_hdr_len);
	icmp_len = ntohs(ip_hdr->total_len) - ip_hdr_len;

	/* 检查校验和 */
	if (checksum((unsigned short *)icmp_hdr, icmp_len)){
		printf("[Ping] icmp checksum error!\n");
		return -1;
	}

	/* 检查 ICMP 类型 */
	if (icmp_hdr->type != ICMP_TYPE_ECHO_REPLY){
		printf("[Ping] not echo reply : %d\n", icmp_hdr->type);
		return -1;
	}

	/* 检查 ICMP 的 ID */
	if (icmp_hdr->id != (unsigned short)GetCurrentProcessId()){
		printf("[Ping] someone else's message!\n");
		return -1;
	}

	recv_ipid = htons(ip_hdr->id);

	if ((recv_ipid - *ipid > 10) && *ipid)
		recv_data = 1;
	else
		recv_data = 0;
	*ipid = recv_ipid;
	//	printf("ipid:%d ", recv_ipid);
	return recv_data;
}

unsigned short checksum(unsigned short *buf, int buf_len){
	unsigned long checksum = 0;
	while (buf_len > 1){
		checksum += *buf++;
		buf_len -= sizeof(unsigned short);
	}
	if (buf_len){
		checksum += *(unsigned char *)buf;
	}
	checksum = (checksum >> 16) + (checksum & 0xffff);
	checksum += (checksum >> 16);
	return (unsigned short)(~checksum);
}