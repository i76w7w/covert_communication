void setup_winsock();
void create_socket(SOCKET *icmp_soc);
void set_socket(SOCKET *icmp_soc, struct sockaddr_in *dest_addr, icmp_user_opt* user_opt, unsigned int ip_addr);
void resolve_ip_address(unsigned int *ip_addr, icmp_user_opt* user_opt);
void parse_parameter(int argc, char **argv, icmp_user_opt *user_opt, SYNC* ctl_sync);
void help(char *prog_name);