#include<stdio.h>
#include<packet.h>
#include<handle_recv.h>
#include<string.h>
void sync(RECV *recv, SYNC*ctl_sync){
	int p_to_byte = ctl_sync->p_to_byte;
	int p_to_bit = ctl_sync->p_to_bit;
	if (recv->recv_bit == (((ctl_sync->check[p_to_byte]) >> p_to_bit) & 1)){
		if (p_to_bit == 7){
			if (p_to_byte == strlen(ctl_sync->check) - 1){
				ctl_sync->flag = 1;
				return;
			}
			ctl_sync->p_to_byte++;
			ctl_sync->p_to_bit = 0;
			return;
		}
		ctl_sync->p_to_bit++;
		return;
	}
	ctl_sync->p_to_byte = 0;
	ctl_sync->p_to_bit = 0;
	return;
}
void handle_recv(RECV *recv){
	if (recv->recv_bit){
		recv->recv_char += 1 << recv->count_bit;
	}
	recv->count_bit++;
	if (recv->count_bit == 8){
		printf(" %c", recv->recv_char);
		recv->recv_char = 0;
		recv->count_bit = 0;
	}
}